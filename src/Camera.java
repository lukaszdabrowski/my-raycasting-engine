import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Camera implements KeyListener {

    public double xPos, yPos, xDir, yDir, xPlane, yPlane;
    public boolean left, right, forward, back;
    public final double MOVE_SPEED = 0.08;
    public final double ROTATION_SPEED = 0.045;

    public Camera(double x, double y, double xD, double yD, double xP, double yP) {
        xPos = x;
        yPos = y;
        xDir = xD;
        yDir = yD;
        xPlane = xP;
        yPlane = yP;
    }

    public void keyTyped(KeyEvent arg0) {
        // implementacja wymuszona przez interfejs KeyListener
    }

    public void keyPressed(KeyEvent key) {
        int keyCode = key.getKeyCode();
        if ((keyCode == 37)) {
            left = true;
        } else if ((keyCode == 39)) {
            right = true;
        } else if ((keyCode == 38)) {
            forward = true;
        } else if ((keyCode == 40)) {
            back = true;
        }
    }

    public void keyReleased(KeyEvent key) {
        left = false;
        right = false;
        forward = false;
        back = false;
    }

    public void update(int[][] map) {
        if (forward) {
            if (map[(int) (xPos + xDir * MOVE_SPEED)][(int) yPos] == 0) {
                xPos += xDir * MOVE_SPEED;
            }
            if (map[(int) xPos][(int) (yPos + yDir * MOVE_SPEED)] == 0) {
                yPos += yDir * MOVE_SPEED;
            }
        }
        if (back) {
            if (map[(int) (xPos - xDir * MOVE_SPEED)][(int) yPos] == 0) {
                xPos -= xDir * MOVE_SPEED;
            }
            if (map[(int) xPos][(int) (yPos - yDir * MOVE_SPEED)] == 0) {
                yPos -= yDir * MOVE_SPEED;
            }
        }
        if (right) {
            double oldxDir = xDir;
            xDir = xDir * Math.cos(-ROTATION_SPEED) - yDir * Math.sin(-ROTATION_SPEED);
            yDir = oldxDir * Math.sin(-ROTATION_SPEED) + yDir * Math.cos(-ROTATION_SPEED);
            double oldxPlane = xPlane;
            xPlane = xPlane * Math.cos(-ROTATION_SPEED) - yPlane * Math.sin(-ROTATION_SPEED);
            yPlane = oldxPlane * Math.sin(-ROTATION_SPEED) + yPlane * Math.cos(-ROTATION_SPEED);
        }
        if (left) {
            double oldxDir = xDir;
            xDir = xDir * Math.cos(ROTATION_SPEED) - yDir * Math.sin(ROTATION_SPEED);
            yDir = oldxDir * Math.sin(ROTATION_SPEED) + yDir * Math.cos(ROTATION_SPEED);
            double oldxPlane = xPlane;
            xPlane = xPlane * Math.cos(ROTATION_SPEED) - yPlane * Math.sin(ROTATION_SPEED);
            yPlane = oldxPlane * Math.sin(ROTATION_SPEED) + yPlane * Math.cos(ROTATION_SPEED);
        }
    }
}
